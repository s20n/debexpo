# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2023-06-15 20:52+0000\n"
"Last-Translator: gallegonovato <fran-carro@hotmail.es>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/debexpo/packages/"
"es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.18.1-dev\n"

#: packages/models.py
msgid "Name"
msgstr "Nombre"

#: packages/models.py
msgid "Needs a sponsor?"
msgstr "¿Necesitas un patrocinador?"

#: packages/models.py
msgid "In Debian?"
msgstr "¿En Debian?"

#: packages/models.py packages/templates/packages-list.html
msgid "Version"
msgstr "Versión"

#: packages/models.py
msgid "Changes"
msgstr "Cambios"

#: packages/models.py
msgid "Closes bugs"
msgstr "Corrige los errores"

#: packages/models.py
msgid "Git storage ref"
msgstr "Ref. de almacenamiento en Git"

#: packages/models.py
msgid "Upload date"
msgstr "Fecha de subida"

#: packages/models.py
msgid "Maintainer"
msgstr "Mantenedor"

#: packages/models.py
msgid "Homepage"
msgstr "Página de inicio"

#: packages/models.py
msgid "VCS"
msgstr "VCS"

#: packages/models.py packages/templates/packages-list.html
msgid "Description"
msgstr "Descripción"

#: packages/templates/package.html
#, python-format
msgid "<h1>Details about package %(name)s</h1>"
msgstr "<h1>Detalles sobre el paquete %(name)s</h1>"

#: packages/templates/package.html
msgid "Name:"
msgstr "Nombre:"

#: packages/templates/package.html
msgid "Uploader:"
msgstr "Cargador:"

#: packages/templates/package.html
msgid "Description:"
msgstr "Descripción:"

#: packages/templates/package.html
msgid "Subscribe:"
msgstr "Suscríbete:"

#: packages/templates/package.html
msgid "Edit your subscription"
msgstr "Editar tus suscripciones"

#: packages/templates/package.html
msgid "Needs a sponsor:"
msgstr "Necesita un patrocinador:"

#: packages/templates/package.html packages/templates/packages-list.html
msgid "Yes"
msgstr "Sí"

#: packages/templates/package.html packages/templates/packages-list.html
msgid "No"
msgstr "No"

#: packages/templates/package.html
msgid "Change"
msgstr "Cambiar"

#: packages/templates/package.html
msgid "Delete package:"
msgstr "Borrar el paquete:"

#: packages/templates/package.html
msgid "Admin package deletion"
msgstr "Eliminación de los paquetes de administración"

#: packages/templates/package.html
msgid "Delete this package"
msgstr "Eliminar este paquete"

#: packages/templates/package.html
msgid "Package uploads"
msgstr "Carga de los paquetes"

#: packages/templates/package.html
#, python-format
msgid "Upload #%(index)s"
msgstr "Cargar #%(index)s"

#: packages/templates/package.html
msgid "Admin upload deletion"
msgstr "Eliminación de las cargas por el administrador"

#: packages/templates/package.html
msgid "Delete this upload"
msgstr "Borrar esta carga"

#: packages/templates/package.html
msgid "Information"
msgstr "Información"

#: packages/templates/package.html
msgid "Version:"
msgstr "Versión:"

#: packages/templates/package.html
msgid "View RFS template"
msgstr "Ver la plantilla RFS"

#: packages/templates/package.html
msgid "Uploaded:"
msgstr "Subido:"

#: packages/templates/package.html
msgid "Source package:"
msgstr "Paquete de fuentes:"

#: packages/templates/package.html
msgid "Distribution:"
msgstr "Distribución:"

#: packages/templates/package.html
msgid "Section:"
msgstr "Sección:"

#: packages/templates/package.html
msgid "Priority:"
msgstr "Prioridad:"

#: packages/templates/package.html
msgid "Homepage:"
msgstr "Página de inicio:"

#: packages/templates/package.html
msgid "Closes bugs:"
msgstr "Corrige los errores:"

#: packages/templates/package.html
msgid "Changelog"
msgstr "Registro de cambios"

#: packages/templates/package.html
msgid "QA information"
msgstr "Información sobre control de calidad"

#: packages/templates/package.html
msgid "Comments"
msgstr "Comentarios"

#: packages/templates/package.html
msgid "Needs work"
msgstr "Necesita reformas"

#: packages/templates/package.html
msgid "Ready"
msgstr "Listo"

#: packages/templates/package.html
msgid "Package has been uploaded to Debian"
msgstr "El paquete se ha subido a Debian"

#: packages/templates/package.html
msgid "No comments"
msgstr "Sin comentarios"

#: packages/templates/package.html
msgid "New comment"
msgstr "Nuevo comentario"

#: packages/templates/package.html
msgid "Submit"
msgstr "Enviar"

#: packages/templates/packages-list.html
msgid "Package"
msgstr "Paquete"

#: packages/templates/packages-list.html
msgid "Uploader"
msgstr "Cargador"

#: packages/templates/packages-list.html
msgid "Needs a sponsor"
msgstr "Necesitas un patrocinador"

#: packages/templates/packages-list.html
msgid "Already in Debian"
msgstr "Ya disponible en Debian"

#: packages/templates/packages-list.html
msgid "No packages"
msgstr "Sin paquetes"

#: packages/views.py
msgid "Today"
msgstr "Hoy"

#: packages/views.py
msgid "Yesterday"
msgstr "Ayer"

#: packages/views.py
msgid "Some days ago"
msgstr "Hace unos días"

#: packages/views.py
msgid "Older packages"
msgstr "Paquetes más antiguos"

#: packages/views.py
msgid "Uploaded long ago"
msgstr "Subido hace tiempo"

#: packages/views.py
msgid "Packages for {} {}"
msgstr "Paquetes para {} {}"

#: packages/views.py
msgid "Package list"
msgstr "Lista de paquetes"

#: packages/views.py
#, python-format
msgid "%s packages"
msgstr "%s paquetes"

#: packages/views.py
#, python-format
msgid "A feed of packages on %s"
msgstr "Una fuente de paquetes en %s"

#: packages/views.py
msgid "Package {} uploaded by {}."
msgstr "Paquete {} subido por {}."

#: packages/views.py
msgid "Uploader is currently looking for a sponsor."
msgstr "Uploader está buscando un patrocinador."

#: packages/views.py
msgid "Uploader is currently not looking for a sponsor."
msgstr "Uploader actualmente no busca patrocinador."
