# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-05 07:56+0000\n"
"PO-Revision-Date: 2022-11-23 14:54+0000\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/debexpo/keyring/"
"fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.15-dev\n"

#: keyring/models.py
msgid "Type"
msgstr "گونه"

#: keyring/models.py
msgid "GPG Algorithm ID"
msgstr "شناسهٔ الگوریتم GPG"

#: keyring/models.py
#, fuzzy
#| msgid "Minimal size requirment"
msgid "Minimal size requirement"
msgstr "نیازمندی کمینهٔ اندازه"

#: keyring/models.py
msgid "Multiple keys not supported"
msgstr "چندین کلید پشتیبانی نشده"

#: keyring/models.py
msgid "Key algorithm not supported. Key must be one of the following: {}"
msgstr "چندین کلید پشتیبانی نشده. کلید باید یکی از موارد زیر باشد: {}"

#: keyring/models.py
msgid "Key size too small. Need at least {} bits."
msgstr "اندازهٔ کلید زیادی کوچک است. نیازمند کمینه {} بیت."

#: keyring/models.py
msgid "OpenGPG key"
msgstr "کلید OpenGPG"

#: keyring/models.py
msgid "Fingerprint"
msgstr "اثر انگشت"

#: keyring/models.py
msgid "Last update on"
msgstr "آخرین به‌روز رسانی در"

#: keyring/models.py
msgid "Size"
msgstr "اندازه"
